#include <iostream>
#include <Python.h>

int main()
{
	PyObject* pInt;

	try
	{
		Py_Initialize();
	}
	catch (std::exception e)
	{
		printf("%s", e.what());
	}

	PyRun_SimpleString("print('Hello World from Embedded Python!!!')");

	Py_Finalize();

	printf("\nPress any key to exit...\n");
	if(!getchar()) getchar();
	return 0;
}